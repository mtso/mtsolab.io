# mtso.gitlab.io

## Build

### Jekyll

```
export JEKYLL_VERSION=minimal
docker run --rm \
  --volume="$PWD:/srv/jekyll" \
  -it jekyll/jekyll:$JEKYLL_VERSION \
  jekyll build
```

### Slides

```
go get github.com/mtso/represent
go install represent
```

```
represent -src talks/ -publish public/
```

## Deploy

1. Build `talks/`
2. Commit `public/`
3. Push to `master`

## Development

```
docker run --name jekylldev --volume="$PWD:/srv/jekyll" -itd -p 4000:4000 jekyll/jekyll:minimal jekyll serve --watch --port 4000
```

### Interactive
```
docker run --name jekylldev --volume="$PWD:/srv/jekyll" -itd -p 4000:4000 jekyll/jekyll:minimal bash
# In another shell
docker exec -it bash
```
