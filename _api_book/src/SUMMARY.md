# Summary

- [HTTP Resources]()
    - [Chapter 1](./chapter_1.md)
    - [Resource Naming](./resource_naming.md)

- [Patterns]()
    - [Application Pattern](./application_pattern.md)

- [References](./references.md)
