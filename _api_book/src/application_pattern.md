# Application Pattern

The application pattern is useful when modeling the creation of a resource
that requires server-side processing between the submission of data and the
creation of a resulting resource.

An application resource captures data that may be used to create a requested
resource or may be used during the review of the application. It is useful to
capture data used during the review that may not be stored as properties on
the requested resource once the application is accepted and requested resource
is provisioned.

## Example

```json
POST /storefront-applications
{
  "storefrontName": "Top Cookies",
  "storefrontDescription": "We bake the freshest cookies to warm your heart.",
  "storefrontOwner": "usr_nfsda09fd8safdsa"
}

Response 201:
{
  "id": "sap_fen2fd9s0afdsaf",
  "storefrontName": "Top Cookie",
  "storefrontDescription": "We bake the freshest cookies to warm your heart.",
  "storefrontOwner": "usr_nfsda09fd8safdsa",
  "storefrontId": null,
  "applicationStatus": "PENDING"
}
```

```json
GET /storefront-applications/sap_fen2fd9s0afdsaf
{
  "id": "sap_fen2fd9s0afdsaf",
  "storefrontName": "Top Cookie",
  "storefrontDescription": "We bake the freshest cookies to warm your heart.",
  "storefrontOwner": "usr_nfsda09fd8safdsa",
  "storefrontId": "stf_hfnkwdjfd02jfels",
  "applicationStatus": "ACCEPTED"
}
```
