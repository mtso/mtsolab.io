# Chapter 1

# Resource IDs

Resource IDs MUST be globally unique.

Resource IDs MUST NOT be incrementing integers.

- Resource IDs should not provide information about the cardinality of a resource.
- Resource ID generation should not require synchronization.

Resource IDs SHOULD be time-sortable.

Resource IDs MAY have a prefix descriptor describing the resource type. The prefix descriptor SHOULD be 3 characters in length and MUST NOT exceed 4 characters in length.

Resource IDs MUST NOT contain `-` characters.
- This prevents breaks up most double-click selection behavior.

Resource IDs for a resource type MUST NOT change from instance to instance.
API integrations store references to resource IDs and increasing the length
of resource IDs risks breaking existing integrations.

## Guidelines

Resource IDs can be generated via ULID or TSID algorithms.
