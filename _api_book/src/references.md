# References
- [https://www.codejam.info/2022/06/empty-body-no-body-http2.html](https://www.codejam.info/2022/06/empty-body-no-body-http2.html)
- [https://docs.aws.amazon.com/AmazonS3/latest/userguide/RESTRedirect.html](https://docs.aws.amazon.com/AmazonS3/latest/userguide/RESTRedirect.html)

- [https://web.archive.org/web/20230401165858/https://blog.atomist.com/kubernetes-apply-replace-patch/](https://web.archive.org/web/20230401165858/https://blog.atomist.com/kubernetes-apply-replace-patch/)
- [https://kubernetes.io/docs/tasks/manage-kubernetes-objects/update-api-object-kubectl-patch/](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/update-api-object-kubectl-patch/)
