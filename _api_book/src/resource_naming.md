# Resource Naming

Resource names for first-class resources MUST be a noun.

Resource names MUST be pluralized.

If a resource name is composed of multiple words, the words MUST be separated by a `-`
hyphen character.

## HTTP Endpoints

HTTP endpoints MUST have JSON request and response bodies.

The top-level schema of request and response payloads MUST be objects.

## Object Fields

Field names MUST be camel-case.

Field names MUST be nouns.
