---
description: Book trading network.
category: code
tags: Go, PostgreSQL, React, Redux
order: 2

links:
  - Live: https://cc7.herokuapp.com
  - Source: https://github.com/mtso/booker
---

Book collecting and trading web app built on the GRRP stack (Go, React, Redux, PostgreSQL).<!--  [Read more](/post/booker). -->
