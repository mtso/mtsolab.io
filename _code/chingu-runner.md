---
description: Slack bot to manage daily sprint todos and broadcast team progress channel-wide.
category: code
tags: Node.js, MongoDB, Slack Bot API
order: 11

links:
  - Live: https://chingurunner.herokuapp.com
  - Source: https://github.com/mtso/chingu-runner
---

Slack bot to manage daily sprint todos and broadcast team progress channel-wide. The bot commands are implemented as controllers that use a common function interface, which aided in choosing the right command when responding to the client's request.
