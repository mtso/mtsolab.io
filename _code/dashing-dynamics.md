---
description: A top down infinite dashing platformer.
category: code
tags: iOS, Swift, SpriteKit
order: 3

links:
  - App Store: https://itunes.apple.com/us/app/dashing-dynamics/id1253316480
  - Source: https://github.com/seungprk/DashingDynamics
---

A top down infinite dashing platformer built on SpriteKit and GameplayKit
in Swift. I was in charge of building the control system, the components,
and scene UIs.
