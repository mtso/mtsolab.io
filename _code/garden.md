---
description: Rogue-like garden stroller.
category: code
tags: React, Redux
order: 4

links:
  - Live: https://mtso.github.io/garden
  - Source: https://github.com/mtso/garden
---

Rogue-like garden stroller. React rendered the ASCII text interface, and Redux ran the game loop logic. A minimum spanning tree linked the generated rooms to bring order to an ever-growing maze.
