---
title: "Kaomojidle"
description: Wordle kaomoji-fied.
category: code
tags: React
order: -5

links:
  - Live: https://mtso.github.io/kaomojidle/
  - Source: https://codesandbox.io/s/kaomojidle-0v5rt
---

A derivative of [wordle](https://www.nytimes.com/games/wordle/) with a word bank containing kaomoji.
Because not all players may know all possible kaomoji, valid next characters are highlighted as the player enters them in.
