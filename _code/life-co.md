---
title: "Life Co."
description: Location check-in coordinator.
category: code
tags: React, React-Router, Express, MySQL
order: 12

links:
  - Live: https://cc5.herokuapp.com
  - Source: https://github.com/mtso/life-co
---

A location check-in coordinator that shows who else is planning to go
to a restaurant on any day. Consumes the Yelp API for location search and
Twitter API for user identity.
