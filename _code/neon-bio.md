---
description: Art gallery creator.
category: code
tags: Node.js, MongoDB, React.js, Puppeteer
order: 1
---

Neon Bio is an online art gallery creator. Users are authenticated through a one-time login
password distributed to NeonMob inboxes through a messaging microservice. On the frontend,
the editor allows ordering of images through an intuitive drag and drop interface.
