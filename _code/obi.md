---
title: "Obi"
description: Obi is a programming language inspired by koka and lua.
category: code
tags: Programming Language
order: -20

links:
  - Live: https://obilang.org/
  - Source: https://github.com/mtso/obi
  - Package Manager: https://github.com/mtso/obipub
---

Obi is a programming language inspired by koka and lua.
Key features are trailing lambdas and match expressions as the only control flow construct.
Obi Pub is a package manager for Obi written in Obi.
