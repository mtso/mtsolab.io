---
title: "Which Side Are You On?"
description: Which Side Are You On? helps an event organizer to run a group game that asks questions and has participants joining one side or the other.
category: code
tags: React, WebSocket, MongoDB, Express
order: -10

links:
  - Live: https://sides.mtso.io/
  - Source: https://github.com/mtso/sides
---

*Which Side Are You On?* helps an event organizer to run a group game that asks questions and has participants joining one side or the other.
