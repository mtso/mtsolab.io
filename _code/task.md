---
description: Distributed git-backed project management.
category: code
tags: C++, SHA-1, Git
order: 9

links:
  - Source: https://github.com/mtso/task
---

Distributed git-backed project management cli. Implemented the hash table
and led the team in the design of the module architecture.
