---
description: Simple key-value store.
category: code
tags: Go, key-value store

published: false

livelink: https://www.dropbox.com/s/73iq8oisbnpdh2t/penguin-jump.mov?dl=0
sourcelink: https://github.com/mtso/jump
---

A top down infinite platformer.
