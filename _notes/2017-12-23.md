---
title: Will You Mentor Me?
date: 2017-12-23
original_url: https://pindancing.blogspot.com/2010/12/answer-to-will-you-mentor-me-is.html
---

Smart, busy people are much more likely to be piqued by actual work done to show my own interest.
