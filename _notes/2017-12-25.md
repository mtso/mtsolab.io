---
title: How to Write Articles and Essays Quickly...
date: 2017-12-25
original_url: http://www.downes.ca/post/38526
---

Fascinating post on structuring “discursive” writing as one goes along. Value prop: first draft is the last draft. The key is to write a leading second paragraph (first is just a hook) that the rest of the essay builds on. Downes lists his four article types: Argument, Explanation, Definition, and Description. He then goes on describe their unique outlines in detail.
