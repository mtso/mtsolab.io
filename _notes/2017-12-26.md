---
title: An Intro to Microservice-based Architecture Through Story
date: 2017-12-26
original_url: https://hackernoon.com/an-introduction-to-microservice-based-architecture-through-story-part-1-55c553ac4bd9
---

Tells a story about the value of approaching a web app with a microservice approach. Describes the development process up to deployment. Good overview to get a list of buzzwords and key terms for Google-ing later. [Part 2](https://medium.com/@elliot_f/an-introduction-to-microservice-based-architecture-through-story-part-2-dacceaff9a13) reuses the same storytelling method to describe maintaining the microservice system.
