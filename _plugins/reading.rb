module Reading
  class ReadingGenerator < Jekyll::Generator
    safe true
    priority :low

    def generate(site)

      # site.categories.each do |category, posts|
      #   print("building cat ", category, "\n")
      #   site.pages << ReadingPage.new(site, category, posts)
      #   posts.each do |post|
      #     site.pages << TestPage.new(site, post)
      #   end
      # end

      # site.pages.each do |page|
      #   if !defined?(page.is_test)
      #     site.pages << TestPage.new(site, page)
      #   end
      # end

      # site.posts.docs.each do |post|
      #   site.pages << TestPage.new(site, post)
      # end
    end
  end

# #!/bin/sh

# for filename in $PAGE_DIR/*.md; do
#   pagename=${filename##*/}
#   pagename=${pagename%.*}
#   echo $pagename
#   echo "<pre>$(git log -p --follow --pretty=format:"Commit: %H%nDate: %cd%n%n%w(80,4,4)%B" $filename)</pre>" > $OUTPUT_DIR$pagename.history.html
# done

  class TestPage < Jekyll::Page
    attr_accessor :is_test

    def initialize(site, page)
      @is_test = true
      @site = site             # the current site instance.
      @base = site.source      # path to the source directory.
      @dir  = 'p'         # the directory the page will reside in.

      # All pages have the same filename, so define attributes straight away.
      n = page.path
      if n.nil?
        n = page.name
      end
      @basename = n     # filename without the extension.
      @ext      = '.history.html'      # the extension.
      @name     = "#{ n }.history.html" # basically @basename + @ext.
      @title = basename

      history = %x[ git log -p --follow --pretty=format:"Commit: %H%nDate: %cd%n%n%w(80,4,4)%B" #{page.path} ]
      print("initing #{ n } #{ history.length }\n")

      # Initialize data hash with a key pointing to all posts under current category.
      # This allows accessing the list in a template via `page.linked_docs`.
      @data = {
        # 'linked_docs' => posts,
        'some_text' => 'whatchamatcallet',
        'history' => history
      }
      # print("wdir", wdir)

      # Look up front matter defaults scoped to type `categories`, if given key
      # doesn't exist in the `data` hash.
      data.default_proc = proc do |_, key|
        site.frontmatter_defaults.find(relative_path, :categories, key)
      end
    end

    def url_placeholders
      {
        :category   => @dir,
        :basename   => basename,
        :output_ext => output_ext,
      }
    end
  end

  class ReadingPage < Jekyll::Page

    def initialize(site, category, posts)
      @site = site             # the current site instance.
      @base = site.source      # path to the source directory.
      @dir  = category         # the directory the page will reside in.

      # All pages have the same filename, so define attributes straight away.
      @basename = 'index'      # filename without the extension.
      @ext      = '.html'      # the extension.
      @name     = 'index.html' # basically @basename + @ext.

      wdir = %x[ pwd ]
      # Initialize data hash with a key pointing to all posts under current category.
      # This allows accessing the list in a template via `page.linked_docs`.
      @data = {
        'linked_docs' => posts,
        'some_text' => 'whatchamatcallet',
        'wdir' => wdir,
      }
      print("wdir", wdir)

      # Look up front matter defaults scoped to type `categories`, if given key
      # doesn't exist in the `data` hash.
      data.default_proc = proc do |_, key|
        site.frontmatter_defaults.find(relative_path, :categories, key)
      end
    end
    #   @site = site             # the current site instance.
    #   @base = site.source      # path to the source directory.
    #   @dir  = category         # the directory the page will reside in.

    #   # All pages have the same filename, so define attributes straight away.
    #   @basename = 'reading'      # filename without the extension.
    #   @ext      = '.html'      # the extension.
    #   @name     = 'reading.html' # basically @basename + @ext.

    #   # Initialize data hash with a key pointing to all posts under current category.
    #   # This allows accessing the list in a template via `page.linked_docs`.
    #   @data = {
    #     # 'linked_docs' => posts
    #   }

    #   # Look up front matter defaults scoped to type `categories`, if given key
    #   # doesn't exist in the `data` hash.
    #   # data.default_proc = proc do |_, key|
    #   #   site.frontmatter_defaults.find(relative_path, :categories, key)
    #   # end
    # end

    def url_placeholders
      {
        :category   => @dir,
        :basename   => basename,
        :output_ext => output_ext,
      }
    end
  end
end
