---
date: dec 31 2016
tags: programming, go
---

<div class="notice">
  Update: This blog is no longer built using <code>lynx</code>. I have switched over to a custom Jekyll template, which lets me focus on the website content instead.
</div>

I've been learning the Go programming language for the past month. It has been a really good exercise in expanding my understanding of programming patterns and constructs. I found the syntax difficult to pick up at first, since function prototypes are the reverse of languages that I am familiar with (C++, Swift, Javascript). Single character variable names in many example Go projects further decreased my comprehension speed when reading code. A concept that caught me off guard was concurrency. It was the first time that I had tried a language where concurrent patterns were such an integral part of the language.

It was a good learning experience, though. Once past the initial learning curve, Go has been a fun and productive language. The removal of classes and class inheritance flattens project structure and splits larger projects into smaller packages. Smaller packages means more cohesive objects and better code reusability.

As a way to practice using Go and its standard web libraries, I have been building small projects. I started off with a couple microservices using go-kit. Each service is very simple in order to perserve "new-project" momentum as a solo developer. I envision that as more microservices are built, they could work together to provide more complex functionality. For example, the [ulog](https://github.com/mtso/ulog) URI-logging microservice exposes two endpoints. A POST endpoint saves a URI and an optional description, which is internally associated with a timestamp and serial number. A GET endpoint returns all the URIs that have been saved thus far encoded as JSON.

For my most recent project, I built a simple static site generator named lynx. The code is hosted on Github [here](https://github.com/mtso/lynx). It is the generator that is used to build this blog. I had been sketching website layouts using bare HTML and CSS. These [index](http://mtso.gitlab.io/blog-sketch/) and [post](http://mtso.gitlab.io/blog-sketch/post.html) layouts were inspired by the aesthetics of bare websites like [info.cern.ch](http://info.cern.ch) and [stallman.org](https://stallman.org/). I wanted to see how far I could go using the default browser font and using as little style code as possible. Most of the time, the default is the serif Times New Roman, which I think looks nice with the large title size.

Getting the markdown content files to render into valid HTML properly was more difficult than I thought. I believe this is because of the good rendering security of Go's html/template package. String values that contain HTML cannot be directly executed onto a template—the template package will encode the tags so that it shows up as plain text. In order to get template to recognize the HTML tags, the HTML string needs to be parsed as a template, which is then nested into the final template for the webpage.

One of my goals for starting a blog is to improve my writing. I want to be able to describe concepts using clear and easily understandable writing. So I have added a crude Flesch-Kincaid grade level analyzer to the site generator. It evaluates words per sentence and syllables per word to come up with a score. The score is an indicator of the grade level of education that an average person needs to understand the analyzed text. So lower is better if one writes to reach as large an audience as possible. I recognize that writing quality cannot be fully measured by one tool, so I hope to use the Flesch-Kincaid algorithm as one metric of a few to measure my progress.

**List of 👌 Websites**
: [https://xph.us]()
: [https://stallman.org]()
: [https://research.swtch.com]()
: [http://openmymind.io]()
: [https://blog.nelhage.com]()
: [http://wiki.c2.com]()
: [http://www.stephendiehl.com/posts.html]()
: [https://news.ycombinator.com]()
: [http://ilyabirman.net/meanwhile/]()
: [http://loup-vaillant.fr/articles]()
: [https://neil.fraser.name/news]()
: [https://brennan.io/blog]()
: [http://seldomlogical.com]()
: [http://nondot.org/sabre/]()
