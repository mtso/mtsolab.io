---
date: Jan 1 2017
tags: 🃏tabletop games
published: false

outline:
  - Rule addition to the Apples to Apples board game.
---

I recently played Apples to Apples at a friend's house. About the time when the first person reached 10 green cards, we came up with an idea to spice up the game. Instead of putting one card down to match each green card, we would put down two cards. This allowed for the forming of simple phrases and, occasionally, complete sentences. We placed the cards down in the order that they would appear once flipped. In other words, when the cards are initially placed upside-down (to hide who put down which cards), the bottom card would be considered first when read and the top card would be considered second.

It turns out that the original Apples to Apples rules contains a list of variations on the normal rules, too. Our variation is the inverse of the "2-for-1 Apples" variation.

---

### Apples Plus Apples Variation Rules

The game plays as normal with the [standard quick-play ruleset](http://www.boardgamer.ro/custom_images/regulamente/Regulament%20Apples%20to%20Apples.pdf) with the following changes:

1. At the start of the game, the judge deals nine (9) red cards per player.
2. Players choose two red cards, instead of one, for each turn.
        1. The two cards should be placed face-down on the table, one on top of the other.
        2. The bottom card will become the first card when both are flipped together.
        3. When the judge flips a set of cards, the card in front will be read first.
3. At the beginning of each turn, the judge deals enough red apple cards to bring each player's hand back up to nine (9).

---

More descriptions of Apples to Apples variations can be found at the board games forum [boardgamegeek.com](https://boardgamegeek.com/thread/51762/apples-apples-variations) and this Apples to Apples fan-site [com-www.com/applestoapples](http://www.com-www.com/applestoapples/applestoapples-rules-variations.html).
