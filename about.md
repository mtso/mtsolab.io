---
layout: layout
title: "mtso.io | about"
permalink: /about/
nav_order: 9
---

<img class="half-width" src="/assets/img/why@2x.jpg">

I'm a full-stack software engineer with a keen eye for building
enjoyable product experiences. Learning new technologies,
overcoming technical challenges, and building the best ideas
gets me up in the morning.

<br>

[GitHub](https://github.com/mtso)
[LinkedIn](http://linkedin.com/in/matthewtso/)
