// mtso 2017
//
// if on index page, load /code/

window.addEventListener('load', function() {
  if (location.pathname === '/') {
    SiteRouter.redirect('/code/');
  }
});

// Loader code
SiteRouter.addEventListener('loadstart', startLoader);
SiteRouter.addEventListener('loadend', stopLoader);

function startLoader() {
  var node = document.querySelector('.typemark');
  node.setAttribute(
    'class',
    node.getAttribute('class')
      .split(' ')
      .concat('loader')
      .join(' ')
  );
}

// Don't need this if all of document.body is replaced
function stopLoader() {
  var node = document.querySelector('.typemark');
  node.setAttribute(
    'class',
    node.getAttribute('class')
      .split(' ')
      .filter(function(cl) { return cl !== 'loader' })
      .join(' ')
  );
}
